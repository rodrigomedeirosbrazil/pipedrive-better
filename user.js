// ==UserScript==
// @name         Pipedrive better
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://lejour3.pipedrive.com/pipeline/*
// @grant        none
// @require http://code.jquery.com/jquery-latest.js
// ==/UserScript==

(function() {
    'use strict';

    function better(){
        console.log('carregamento completo');
        var checkfront =
        setInterval(function(){
            //$('.front').css('background-color', 'red');
            console.log('procurando front...');
            var fronts = $( ".front" );
            if(fronts.length>0){
                console.log('achou fronts!');
                $( fronts ).each(function( index ) {
                    if(!$( this ).hasClass('alterado')){
                        var st = $( this ).find('strong');
                        var sm = $( this ).find('small');
                        $(sm).insertBefore($(st));
                        $(sm).css({
                            'font-weight': 'bold',
                            'color':'blue'
                        });
                        $( this ).addClass('alterado');
                    }
                });
                //clearInterval(checkfront);
            } else console.log('NAO achou fronts...');
        }, 1000);
    }

    $(document).ready(function() {
        better();
    });

})();
